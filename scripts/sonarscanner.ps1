Set-StrictMode -Version Latest
$ErrorActionPreference = "Stop"
$PSDefaultParameterValues['*:ErrorAction'] = 'Stop'

#& dotnet new tool-manifest
#& dotnet tool install --global dotnet-sonarscanner
#& nuget restore -NoCache  # restore Nuget dependencies
if ([string]::IsNullOrEmpty("$env:CI_MERGE_REQUEST_IID"))
{
    & echo "Running Branch analysis"
    & dotnet sonarscanner begin `
          /o:"develop215" `
          /k:"develop215_dotnetsample" `
          /d:sonar.login="f723af970fedb9e2751305b96af759019f31c64b" `
          /d:sonar.host.url="https://sonarcloud.io" `
          /d:sonar.qualitygate.wait=true `
          /d:sonar.branch.name="$env:CI_COMMIT_BRANCH"
}
else
{
    & echo "Running SQ Merge Request analysis"
    & dotnet sonarscanner begin `
          /o:"develop215" `
          /k:"develop215_dotnetsample" `
          /d:sonar.login="f723af970fedb9e2751305b96af759019f31c64b" `
          /d:sonar.host.url="https://sonarcloud.io" `
          /d:sonar.qualitygate.wait=true `
          /d:sonar.pullrequest.key="$env:CI_MERGE_REQUEST_IID" `
          /d:sonar.pullrequest.branch="$env:CI_MERGE_REQUEST_SOURCE_BRANCH_NAME" `
          /d:sonar.pullrequest.base="$env:CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
}

& "$env:MSBUILD_PATH" `
    .\dotnetsample\dotnetsample.csproj -t:Build  # build the project
& dotnet sonarscanner end `
    /d:sonar.login="f723af970fedb9e2751305b96af759019f31c64b"